# wsproxy handler

## Setup dev environment

### Requirements

*   `go^1.7`
*   `docker^1.12`
*   `docker-compose^1.8.0-rc1`

## Build

```
make
```

## Test

```
make test
```

## Run

Start requirement services
```
docker-compose up
```

Run
```
make run
```

Integration tests
```
go test -tags=integration
```
