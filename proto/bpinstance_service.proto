syntax = "proto3";
/**
 * gRPC interface of Business Process Instance Aggregate
 *
 * Service url `aggr_bpinstance.service.consul:17004`
 */
package pb;

/**
 * Service healthcheck request
 */
message ABPIStatusRq {}
/**
 * Service healthcheck response
 */
message ABPIStatusRs {
    string service = 1;
    string project = 2;
    string version = 3;
    int64  uptime  = 4;
}

/**
 * Request to generate empty XPDL
 */
message ABPINewRq {
    string id   = 1;
    string name = 2;
}
/**
 * Empty XPDL with given Id and Name
 */
message ABPINewRs {
    string xPDL = 1;
}

/**
 * Request to validate XPDL
 */
message ABPIValidateRq {
    string xPDL = 1;
}
/**
 * Validation result. If no errors, return list of used groups
 */
message ABPIValidateRs {
    repeated string errors = 1;
    repeated string groups = 2;
}

/**
 * Create instance based on XPDL
 */
message ABPIRunRq {
    string xPDL = 1;
}
/**
 * Id of new instance
 */
message ABPIRunRs {
    string id = 1;
}

/**
 * Request to close task on instance
 */
message ABPICloseTaskRq {
    string id     = 1;
    string taskId = 2;
    string data   = 3;
    string exit   = 4;
}
/**
 * There is no data return after closing task
 */
message ABPICloseTaskRs {}

/**
 * Business Process Instance Aggregate
 *
 * Service to parsing, validation and execution XPDL
 */
service ABPInstance {
    rpc Status    (ABPIStatusRq)    returns (ABPIStatusRs)    {}  /// This is a private method used for smart load ballancing in future. Don't provide this to customers
    rpc New       (ABPINewRq)       returns (ABPINewRs)       {}  /// This is a private method used by `aggr_bptemplate`. Don't provide this to customers
    rpc Validate  (ABPIValidateRq)  returns (ABPIValidateRs)  {}  /// This is a private method used by `aggr_bptemplate`. Don't provide this to customers
    rpc Run       (ABPIRunRq)       returns (ABPIRunRs)       {}  /// This is a private method used by `aggr_bptemplate`. Don't provide this to customers
    rpc CloseTask (ABPICloseTaskRq) returns (ABPICloseTaskRs) {}  /// Close task that has been created on XPDL execution flow
}
