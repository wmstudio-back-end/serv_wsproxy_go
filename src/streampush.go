package wsproxy

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"

	"git.elewise.com/elma365/serv_wsproxy/src/pb"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	"golang.org/x/net/context"
)

var notifications = struct {
	sync.RWMutex
	m map[string]context.CancelFunc
}{
	m: map[string]context.CancelFunc{},
}

type notificationMessage struct {
	Key    *pb.Notification_Key    `json:"key"`
	Header *pb.Notification_Header `json:"header"`
	Body   *json.RawMessage        `json:"body"`
}

// Create queue and notify assigned sessions
func StartNotify(company string) error {
	notifications.RLock()
	if _, ok := notifications.m[company]; ok {
		notifications.RUnlock()

		return nil
	}
	notifications.RUnlock()
	notifications.Lock()
	defer notifications.Unlock()
	if _, ok := notifications.m[company]; ok {
		notifications.RUnlock()

		return nil
	}

	l := log.New(os.Stdout, company+": ", log.Flags())

	l.Print("open mq connection")

	url := fmt.Sprintf(
		"amqp://%s:%s@rabbitmq.service.consul:5672/%s",
		viper.GetString("rabbitmq.user"),
		viper.GetString("rabbitmq.pass"),
		company,
	)
	cn, err := amqp.Dial(url)
	if err != nil {
		l.Printf("mq: %s", err)

		return err
	}

	ch, err := cn.Channel()
	if err != nil {
		cn.Close()
		l.Printf("mq: %s", err)

		return err
	}

	if err := ch.ExchangeDeclare("notifications", "fanout", false, false, false, false, nil); err != nil {
		ch.Close()
		cn.Close()
		l.Printf("mq: %s", err)

		return err
	}

	// setup exclusive queue
	q, err := ch.QueueDeclare("", false, true, true, false, nil)
	if err != nil {
		ch.Close()
		cn.Close()
		l.Printf("mq: %s", err)

		return err
	}
	if err := ch.QueueBind(q.Name, "", "notifications", false, nil); err != nil {
		ch.Close()
		cn.Close()
		l.Printf("mq: %s", err)

		return err
	}

	msgs, err := ch.Consume(q.Name, "serv_wsproxy", true, true, false, false, nil)
	if err != nil {
		ch.Close()
		cn.Close()
		l.Printf("mq: %s", err)

		return err
	}

	ctx, cancel := context.WithCancel(context.Background())
	notifications.m[company] = cancel

	go func() {
		defer cn.Close()
		defer ch.Close()

		for {
			select {
			case <-ctx.Done():

				return

			case msg, ok := <-msgs:
				if !ok {
					l.Printf("mq connection lost")

					return
				}
				b := bytes.NewBuffer(msg.Body)
				n := &pb.Notification{}
				if err := jsonpb.Unmarshal(b, n); err != nil {
					l.Printf("proto: %s", err)

					break
				}
				data := json.RawMessage(n.Body)

				nm := &notificationMessage{
					Key:    n.Key,
					Header: n.Header,
					Body:   &data,
				}

				if sessions.ig[company] == nil {

					break
				}
				ns := map[string]*Session{}
				for _, g := range n.Groups {
					for id, s := range sessions.ig[company][g] {
						ns[id] = s
					}
				}
				for _, s := range ns {
					s.Send(0, "notification", nm, nil)
				}
			}
		}
	}()

	return nil
}
