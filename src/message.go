package wsproxy

import "encoding/json"

type MessageRq struct {
	Data      json.RawMessage `json:"data"`
	RequestId int64           `json:"requestId"`
	Method    string          `json:"method"`
	Company   string          `json:"company"`
}
