package switcher

import (
	"errors"
	"io"

	"git.elewise.com/elma365/serv_wsproxy/src/pb"

	"github.com/gogo/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"gopkg.in/inconshreveable/log15.v2"
)

var aoscl pb.AOrgstructClient

func init() {
	conn, err := grpc.Dial("aggr_orgstruct.service.consul:17002", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	aoscl = pb.NewAOrgstructClient(conn)

	handlers["aggr_orgstruct"] = AOrgstructHandler
}

func AOrgstructHandler(ctx context.Context, key string, r io.Reader) (interface{}, error) {
	switch key {
	case "Update":
		d := &pb.AOSUpdateRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return aoscl.Update(ctx, d)
	}

	return nil, errors.New("unknown method")
}
