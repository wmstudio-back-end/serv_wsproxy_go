package switcher

import (
	"errors"
	"io"

	"git.elewise.com/elma365/serv_wsproxy/src/pb"

	"github.com/gogo/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"gopkg.in/inconshreveable/log15.v2"
)

var amesscl pb.AMessageClient

func init() {
	conn, err := grpc.Dial("aggr_message.service.consul:17005", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	amesscl = pb.NewAMessageClient(conn)

	handlers["aggr_message"] = AMessageHandler
}

func AMessageHandler(ctx context.Context, key string, r io.Reader) (interface{}, error) {
	switch key {
	case "CreateChain":
		d := &pb.AMsgCreateChainRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return amesscl.CreateChain(ctx, d)

	case "AddComment":
		d := &pb.AMsgAddCommentRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return amesscl.AddComment(ctx, d)

	case "MarkRead":
		d := &pb.AMsgMarkReadRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return amesscl.MarkRead(ctx, d)
	}

	return nil, errors.New("unknown method")
}
