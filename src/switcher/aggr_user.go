package switcher

import (
	"errors"
	"io"

	"git.elewise.com/elma365/serv_wsproxy/src/pb"

	"github.com/gogo/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"gopkg.in/inconshreveable/log15.v2"
)

var ausercl pb.AUserClient

func init() {
	conn, err := grpc.Dial("aggr_user.service.consul:17001", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	ausercl = pb.NewAUserClient(conn)

	handlers["aggr_user"] = AUserHandler
}

func AUserHandler(ctx context.Context, key string, r io.Reader) (interface{}, error) {

	switch key {
	case "Create":
		d := &pb.AUserCreateRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return ausercl.Create(ctx, d)

	case "Update":
		d := &pb.AUserUpdateRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return ausercl.Update(ctx, d)

	case "Delete":
		d := &pb.AUserDeleteRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return ausercl.Delete(ctx, d)

	case "Recover":
		d := &pb.AUserRecoverRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return ausercl.Recover(ctx, d)
	}

	return nil, errors.New("unknown method")
}
