package switcher

import (
	"io"

	"git.elewise.com/elma365/serv_wsproxy/src/pb"

	"errors"

	"github.com/gogo/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"gopkg.in/inconshreveable/log15.v2"
)

var abptcl pb.ABPTemplateClient

func init() {
	conn, err := grpc.Dial("aggr_bptemplate.service.consul:17003", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	abptcl = pb.NewABPTemplateClient(conn)

	handlers["aggr_bptemplate"] = ABPTemplateHandler
}

func ABPTemplateHandler(ctx context.Context, key string, r io.Reader) (interface{}, error) {
	switch key {
	case "Run":
		d := &pb.ABPTRunRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return abptcl.Run(ctx, d)

	case "Create":
		d := &pb.ABPTCreateRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return abptcl.Create(ctx, d)

	case "ResetDraft":
		d := &pb.ABPTResetDraftRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return abptcl.ResetDraft(ctx, d)

	case "UpdateDraft":
		d := &pb.ABPTUpdateDraftRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return abptcl.UpdateDraft(ctx, d)

	case "PublishDraft":
		d := &pb.ABPTPublishDraftRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return abptcl.PublishDraft(ctx, d)
	}

	return nil, errors.New("unknown method")
}
