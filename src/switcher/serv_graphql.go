package switcher

import (
	"io"

	"git.elewise.com/elma365/serv_wsproxy/src/pb"

	"errors"

	"github.com/gogo/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"gopkg.in/inconshreveable/log15.v2"
)

var sgraphqlcl pb.SGraphQLClient

func init() {
	conn, err := grpc.Dial("serv_graphql.service.consul:18003", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	sgraphqlcl = pb.NewSGraphQLClient(conn)

	handlers["serv_graphql"] = SGraphqlHandler
}

func SGraphqlHandler(ctx context.Context, key string, r io.Reader) (interface{}, error) {
	switch key {
	case "Query":
		d := &pb.SGQLQueryRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		r, err := sgraphqlcl.Query(ctx, d)
		if err != nil {

			return nil, err
		}

		return []byte(r.Data), err
	}

	return nil, errors.New("unknown method")
}
