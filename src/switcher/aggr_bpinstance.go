package switcher

import (
	"errors"
	"io"

	"git.elewise.com/elma365/serv_wsproxy/src/pb"

	"github.com/gogo/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"gopkg.in/inconshreveable/log15.v2"
)

var abpicl pb.ABPInstanceClient

func init() {
	conn, err := grpc.Dial("aggr_bpinstance.service.consul:17004", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	abpicl = pb.NewABPInstanceClient(conn)

	handlers["aggr_bpinstance"] = ABPInstanceHandler
}

func ABPInstanceHandler(ctx context.Context, key string, r io.Reader) (interface{}, error) {

	switch key {
	case "CloseTask":
		d := &pb.ABPICloseTaskRq{}
		if err := jsonpb.Unmarshal(r, d); err != nil {
			log15.Error("jsonpb", "err", err)

			return nil, err
		}

		return abpicl.CloseTask(ctx, d)
	}

	return nil, errors.New("unknown method")
}
