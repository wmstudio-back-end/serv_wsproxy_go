package switcher

import (
	"errors"
	"io"
	"strings"

	"golang.org/x/net/context"
)

type Handler func(ctx context.Context, key string, r io.Reader) (interface{}, error)

var handlers = map[string]Handler{}

func Route(ctx context.Context, key string, r io.Reader) (interface{}, error) {
	skey := strings.Split(key, ".")
	if len(skey) != 2 {

		return nil, errors.New("method must be in format <service>.<command>")
	}
	if h, ok := handlers[skey[0]]; ok {

		return h(ctx, skey[1], r)
	}

	return nil, errors.New("service not found")
}

// func Switcher(msg Message, s *WsServer, ws *websocket.Conn) error {

// 	key := strings.Split(msg.Method, ".")

// 	switch key[0] {
// 	case "aggr_message":
// 		responseJSON,err:=switcher.AggrMessage(msg)
// 	case "serv_graphql":
// 		switch key[1] {
// 		case "Query":

// 			_, q := data["q"]
// 			if q {
// 				r, err := s.grpcGraphQl.Query(ctx, &pb.SGQLQueryRq{Q: data["q"]})
// 				responseJSON, errStr := StringifyIntr(r)
// 				respWs(ws, responseJSON, msg, err, errStr)
// 				return nil
// 			}
// 			break
// 		}
// 		break

// 	case "aggr_user":
// 		switch key[1] {
// 		case "Create":
// 			_, username := data["username"]
// 			_, password := data["password"]
// 			_, email := data["email"]
// 			if username && password && email {
// 				r, err := s.grpcUser.Create(ctx, &pb.AUserCreateRq{
// 					Username: data["username"],
// 					Password: data["password"],
// 					Email:    data["email"],
// 				})

// 				responseJSON, errStr := StringifyIntr(r)
// 				respWs(ws, responseJSON, msg, err, errStr)

// 				return nil
// 			}

// 		case "ChangePassword":
// 			_, id := data["id"]
// 			_, password := data["password"]
// 			if id && password {
// 				r, err := s.grpcUser.ChangePassword(ctx, &pb.AUserChangePasswordRq{
// 					Id:       data["id"],
// 					Password: data["password"],
// 				})

// 				responseJSON, errStr := StringifyIntr(r)
// 				respWs(ws, responseJSON, msg, err, errStr)

// 				return nil
// 			}

// 		case "ChangeEmail":
// 			_, id := data["id"]
// 			_, email := data["email"]
// 			if id && email {
// 				r, err := s.grpcUser.ChangeEmail(ctx, &pb.AUserChangeEmailRq{
// 					Id:    data["id"],
// 					Email: data["email"],
// 				})

// 				responseJSON, errStr := StringifyIntr(r)
// 				respWs(ws, responseJSON, msg, err, errStr)
// 				return nil
// 			}

// 		case "Delete":
// 			_, id := data["id"]
// 			if id {
// 				r, err := s.grpcUser.Delete(ctx, &pb.AUserDeleteRq{
// 					Id: data["id"],
// 				})

// 				responseJSON, errStr := StringifyIntr(r)
// 				respWs(ws, responseJSON, msg, err, errStr)

// 				return nil
// 			}

// 		case "Recover":
// 			_, id := data["id"]
// 			if id {
// 				r, err := s.grpcUser.Recover(ctx, &pb.AUserRecoverRq{
// 					Id: data["id"],
// 				})

// 				responseJSON, errStr := StringifyIntr(r)
// 				respWs(ws, responseJSON, msg, err, errStr)

// 				return nil
// 			}
// 		}
// 	default:

// 		break
// 	}
// 	log.Println("invalid data")
// 	log.Println(data)
// 	SendUserWs(ws, []byte("null"), msg, errors.New("invalid data"))
// 	return nil
// }
