package wsproxy

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"net/url"

	"git.elewise.com/elma365/serv_wsproxy/src/switcher"

	"github.com/spf13/viper"
	"golang.org/x/net/websocket"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"gopkg.in/inconshreveable/log15.v2"
)

type Server struct{}

func (s *Server) Conn() {
	http.Handle("/", websocket.Handler(s.listenWebSocket))
	http.ListenAndServe(":"+viper.GetString("wsPort"), nil)
}

func (s *Server) listenWebSocket(ws *websocket.Conn) {
	//  FIXME: after testing company must resolve from host
	// company := strings.Split(ws.Request().URL.Host, '.')[0]
	company := "test"

	log15.Info("open ws...")

	ss := NewSession(ws, company)

	defer ss.Close()

	if token, err := ws.Request().Cookie("token"); err == nil {
		log15.Debug("got token")
		credential, err := url.QueryUnescape(token.Value)
		if err != nil {
			credential = token.Value
		}

		ss.Identify(credential)
	} else {
		ss.Send(0, "serv_auth.Auth", nil, grpc.Errorf(codes.Unauthenticated, "token not found"))
	}
	for {
		rq := &MessageRq{}
		if err := websocket.JSON.Receive(ws, rq); err != nil {
			if err == io.EOF {
				log15.Info("closed")
				ws.Close()

				return
			}
			log15.Error("ws", "err", err)

			ss.Send(0, "unknown", nil, err)

			continue
		}
		if ss.Registered() {
			if rq.Method == "serv_wsproxy.Status" {
				ss.Send(rq.RequestId, rq.Method, []byte(`{"authorized": true}`), nil)
			} else if rq.Method == "serv_wsproxy.Logout" {
				ss.Unregister()
				ss.Send(rq.RequestId, rq.Method, []byte(`{"authorized": false}`), nil)
			} else {
				rs, err := switcher.Route(ss.Context(), rq.Method, bytes.NewReader(rq.Data))
				ss.Send(rq.RequestId, rq.Method, rs, err)
			}
		} else {
			if rq.Method == "serv_auth.Auth" {
				ss.Authenticate(rq.Data)
			} else if rq.Method == "serv_wsproxy.Status" {
				ss.Send(rq.RequestId, rq.Method, []byte(`{"authorized": false}`), nil)
			} else {
				ss.Send(rq.RequestId, rq.Method, nil, errors.New("unknown method (authorized:false)"))
			}
		}
	}
}
