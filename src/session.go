package wsproxy

import (
	"bytes"
	"encoding/json"
	"fmt"
	"sync"

	"git.elewise.com/elma365/serv_wsproxy/src/pb"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/twinj/uuid"
	"golang.org/x/net/context"
	"golang.org/x/net/websocket"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"gopkg.in/inconshreveable/log15.v2"
)

var sessions = struct {
	sync.RWMutex
	ms map[string]*Session
	iu map[string]map[string]*Session            // index by company/user_id
	ig map[string]map[string]map[string]*Session // index by company/group_id/session_id
}{
	ms: map[string]*Session{},
	iu: map[string]map[string]*Session{},
	ig: map[string]map[string]map[string]*Session{},
}

var authcl pb.SAuthClient

func init() {
	conn, err := grpc.Dial("serv_auth.service.consul:18001", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	authcl = pb.NewSAuthClient(conn)
}

type Session struct {
	id         uuid.UUID
	connId     uuid.UUID
	ctx        context.Context
	company    string
	token      string
	userId     uuid.UUID
	groups     []string
	ws         *websocket.Conn
	registered bool
}

func NewSession(ws *websocket.Conn, company string) *Session {

	return &Session{
		connId: uuid.NewV4(),
		ctx: metadata.NewContext(
			context.Background(),
			metadata.Pairs("company", company),
		),
		company: company,
		ws:      ws,
	}
}

func (s *Session) Context() context.Context {
	return s.ctx
}

func (s *Session) Registered() bool {
	return s.registered
}

type MessageRs struct {
	Code      codes.Code       `json:"code"`
	Error     string           `json:"error"`
	RequestId int64            `json:"requestId"`
	Method    string           `json:"method"`
	Data      *json.RawMessage `json:"data"`
}

func (s *Session) Send(id int64, method string, data interface{}, err error) error {
	var d json.RawMessage

	rs := &MessageRs{
		Code:      grpc.Code(err),
		Error:     grpc.ErrorDesc(err),
		RequestId: id,
		Method:    method,
	}

	switch v := data.(type) {
	case []byte:
		d = v
	default:
		d, err = json.Marshal(data)
		if err != nil {
			log15.Error("json", "err", err)

			return err
		}
	}
	rs.Data = &d

	m := json.NewEncoder(s.ws)
	m.SetEscapeHTML(false)
	m.SetIndent("", "  ")
	if err := m.Encode(rs); err != nil {
		log15.Error("json", "err", err)

		return err
	}

	return nil
}

func (s *Session) Authenticate(raw []byte) error {
	d := &pb.SAuthAuthenticateRq{}
	if err := jsonpb.Unmarshal(bytes.NewBuffer(raw), d); err != nil {
		log15.Error("jsonpb", "err", err)
		s.Send(0, "serv_auth.Auth", nil, err)

		return err
	}
	d.StType = pb.Starters_ST_FRONTEND

	ars, err := authcl.Authenticate(s.ctx, d)
	if err != nil {
		log15.Error("serv_auth", "err", err)
		s.Send(0, "serv_auth.Auth", nil, err)

		return err
	}

	return s.Identify(ars.Credential)
}

func (s *Session) Identify(token string) error {
	irs, err := authcl.Identify(s.ctx, &pb.SAuthIdentifyRq{
		Credential: token,
	})
	if err != nil {
		log15.Error("serv_auth", "err", err)
		s.Send(0, "serv_auth.Auth", nil, err)

		return err
	}

	if s.id, err = uuid.Parse(irs.SessionId); err != nil {
		log15.Error("uuid", "err", err)
		s.Send(0, "serv_auth.Auth", nil, err)

		return err
	}
	if s.userId, err = uuid.Parse(irs.UserId); err != nil {
		log15.Error("uuid", "err", err)
		s.Send(0, "serv_auth.Auth", nil, err)

		return err
	}
	s.token = irs.Credential
	md := metadata.Pairs(
		"company", s.company,
		"starter_id", irs.SessionId,
		"starter_type", "FRONTEND",
		"auth_type", "TOKEN",
		"user_id", irs.UserId,
		"credential", irs.Credential,
	)
	s.ctx = metadata.NewContext(context.Background(), md)

	if err := s.Authorize(); err != nil {
		s.Send(0, "serv_auth.Auth", nil, err)

		return err
	}
	for _, group := range s.groups {
		md = metadata.Join(md, metadata.Pairs("groups", group))
	}
	s.ctx = metadata.NewContext(context.Background(), md)

	return s.Send(0, "serv_auth.Auth", irs, nil)
}

func (s *Session) Authorize() error {
	ars, err := authcl.Authorize(s.ctx, &pb.SAuthAuthorizeRq{
		Credential: s.token,
	})
	if err != nil {
		log15.Error("serv_auth", "err", err)

		return err
	}
	s.groups = ars.Groups

	return s.Register()
}

func (s *Session) Register() error {
	sessions.RLock()

	if _, ok := sessions.ms[s.id.String()]; ok {
		sessions.RUnlock()
		log15.Error("duplicate sessions", "id", s.id)

		return fmt.Errorf("duplicate session id '%s'", s.id)
	}

	sessions.RUnlock()
	sessions.Lock()
	defer sessions.Unlock()

	if _, ok := sessions.ms[s.id.String()]; ok {
		log15.Error("duplicate sessions", "id", s.id)

		return fmt.Errorf("duplicate session id '%s'", s.id)
	}

	sessions.ms[s.connId.String()] = s
	if sessions.iu[s.company] == nil {
		sessions.iu[s.company] = map[string]*Session{}
	}
	sessions.iu[s.company][s.userId.String()] = s
	if sessions.ig[s.company] == nil {
		StartNotify(s.company)
		sessions.ig[s.company] = map[string]map[string]*Session{}
	}
	for _, group := range s.groups {
		if sessions.ig[s.company][group] == nil {
			sessions.ig[s.company][group] = map[string]*Session{}
		}
		sessions.ig[s.company][group][s.id.String()] = s
	}
	s.registered = true

	return nil
}

func (s *Session) Unregister() {
	if s.registered {
		sessions.Lock()
		defer sessions.Unlock()

		delete(sessions.ms, s.connId.String())
		delete(sessions.iu[s.company], s.userId.String())
		for _, group := range s.groups {
			delete(sessions.ig[s.company][group], s.id.String())
		}

		ss := NewSession(s.ws, s.company)
		*s = *ss
	}
}

func (s *Session) Close() {
	s.Unregister()

	s.ws.Close()
}
