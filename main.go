package main

import (
	"git.elewise.com/elma365/serv_wsproxy/src"

	"github.com/spf13/viper"
	"gopkg.in/inconshreveable/log15.v2"
)

func main() {
	viper.SetDefault("version", "0.0.1")
	viper.SetDefault("wsPort", "18002")
	viper.SetDefault("wsHost", "localhost")
	viper.SetDefault("rabbitmq.user", "guest")
	viper.SetDefault("rabbitmq.pass", "guest")

	h := log15.CallerFileHandler(log15.StdoutHandler)
	log15.Root().SetHandler(h)

	new(wsproxy.Server).Conn()
	//wsproxy.Server{}.Conn()
}
