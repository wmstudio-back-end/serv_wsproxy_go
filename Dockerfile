FROM alpine:latest

COPY build/serv_wsproxy /srv/serv_wsproxy
WORKDIR /srv/

EXPOSE 18002

ENTRYPOINT ["/srv/serv_wsproxy", "server"]
