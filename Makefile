.PHONY  : build proto data

all     : test build

test    :
	go test -v `go list ./... | grep -v /vendor/`

build   : main.go
	mkdir -p ./build
	env CGO_ENABLED=0 go build -o build/`basename ${PWD}`

run     : main.go
	go run main.go server -lv

proto       :
	mkdir -p ./src/pb
	cd ./proto && protoc --gogofast_out=plugins=grpc:../src/pb *.proto

data        :
	mkdir -p ./src/data
	go-bindata -o src/data/data.go -prefix data -pkg data data
