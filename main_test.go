//+build integration

package main

import (
	"flag"

	"os"

	"testing"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"

	"golang.org/x/net/context"
)

var (
	mock   bool
	local  bool
	golden bool

	ctx context.Context
)

func init() {
	flag.BoolVar(&mock, "mock", false, "use mock instead of real internal services")
	flag.BoolVar(&local, "local", false, "use localhost as address of services")
	flag.BoolVar(&golden, "golden", false, "update golden files")
}

func TestMain(m *testing.M) {
	flag.Parse()
	if local {
		viper.Set("postgres.host", "localhost:5432")
		viper.Set("rabbitmq.host", "localhost:5672")
	} else {
		viper.Set("postgres.host", "postgres:5432")
		viper.Set("rabbitmq.host", "rabbitmq:5672")
	}
	viper.Set("bind", "127.0.0.1:0")

	os.Exit(m.Run())
}
